import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/**
 * Generated class for the RecipesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-recipes',
  templateUrl: 'recipes.html',
})
export class RecipesPage {

  recipes: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private http: Http,
  ) {
    this.http.get('http://e-recipe.dev/v1/recipes/view-all')
      .map(res => res.json())
      .subscribe(
        data => {
          this.recipes = data.data;
          console.log(this.recipes);
        },
        err => {
          console.log("Oops!");
        }
      )
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RecipesPage');
  }

}
