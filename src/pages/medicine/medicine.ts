import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the MedicinePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-medicine',
  templateUrl: 'medicine.html',
})
export class MedicinePage {
  medicine: any;
  details: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private viewCtrl: ViewController,
  ) {
    this.medicine = this.navParams.data;
    this.details = 'summary';
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MedicinePage');
    console.log(this.navParams.data);
  }

  close() {
    this.viewCtrl.dismiss();
  }

}
