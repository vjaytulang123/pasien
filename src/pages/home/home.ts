import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { RecipesPage } from '../../pages/recipes/recipes';
import { MedicinesPage } from '../../pages/medicines/medicines';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }

  openPageRecipes() {
    this.navCtrl.push(RecipesPage);
  }

  openPageMedicines() {
    this.navCtrl.push(MedicinesPage);
  }
}
