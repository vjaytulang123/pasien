import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { MedicinePage } from '../medicine/medicine';

/**
 * Generated class for the MedicinesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-medicines',
  templateUrl: 'medicines.html',
})
export class MedicinesPage {
  medicines: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private modalCtrl: ModalController,
    private http: Http,
  ) {
    this.http.get('http://e-recipe.dev/v1/medicines/view-all')
    .map(res => res.json())
    .subscribe(
      data => {
        this.medicines = data.data;
        console.log(this.medicines);
      },
      err => {
        console.log("Oops!");
      }
    )
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MedicinesPage');
  }

  showDetails(medicine) {
    let modal = this.modalCtrl.create(MedicinePage, medicine);
    modal.present();
  }

}
